package com.fit.pubster;

import com.fit.pubster.entities.Customer;
import com.fit.pubster.repository.CustomerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class PubsterApplication {

	private static final Logger log = LoggerFactory.getLogger(PubsterApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(PubsterApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(CustomerRepository repository) {

		repository.save(new Customer("email4@cvut.cz", "lovesi6",  "Honza", "mylogin", "+420776000000"));

		return (args) -> {
			// save a couple of customers
			repository.save(new Customer("email@cvut.cz", "lovesi1",  "Honza", "mylogin", "+420776000000"));


//			log.info("All customers with email: email@cvut.cz");
//			 for (Customer cust : repository.findByEmail("email@cvut.cz")) {
//			 	log.info(cust.toString());
//			 }
//			log.info("");
		};
	}



}
