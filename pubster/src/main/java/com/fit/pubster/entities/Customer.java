package com.fit.pubster.entities;


import javax.persistence.*;

@Entity
public class Customer {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long CustomerID;
    private String email;
    private String password;
    private String name;
    private String login;
    private String telephone;



    protected Customer() {}

    public Customer(String email, String password, String name, String login, String telephone) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.login = login;
        this.telephone = telephone;
    }


    @Override
    public String toString() {
        return "Customer{" +
                "CustomerID=" + CustomerID +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", login='" + login + '\'' +
                ", telephone='" + telephone + '\'' +
                '}';
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
}
