package com.fit.pubster.base;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public abstract class BaseView extends VerticalLayout {
    public abstract void setupUI();
    public abstract void setupListeners();

    public boolean passwordIsValid(String password) {
        return password.isEmpty() || password.length() < 10;
    }

    public boolean emailIsValid(String email) {
        return email.matches("(.*)@(.*).(.*)");
    }


}
