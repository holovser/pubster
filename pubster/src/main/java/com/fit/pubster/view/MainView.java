package com.fit.pubster.view;

import com.fit.pubster.PubsterApplication;
import com.fit.pubster.base.BaseView;
import com.fit.pubster.entities.Customer;
import com.fit.pubster.repository.CustomerRepository;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.router.Route;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;


/**
 * The main view contains a button and a click listener.
 */
@Route("")
public class MainView extends BaseView {

    private static final Logger log = LoggerFactory.getLogger(PubsterApplication.class);
    private EmailField emailField = new EmailField("e-mail");
    private PasswordField passwordField = new PasswordField("password");
    private RadioButtonGroup<String> userTypeSelect = new RadioButtonGroup<>();
    private Button loginButton = new Button("SIGN IN");
    private Button registerButton = new Button("Do not have an account yet? Sign up!");


    private String[] userTypes = { "customer", "manager", "service" };

    private String widthPercentage = "22%";


    @Autowired
    CustomerRepository repository;


    public MainView() {
        setDefaultHorizontalComponentAlignment(Alignment.CENTER);
        setupUI();
        setupListeners();
    }




    @Override
    public void setupUI() {
        VerticalLayout footerLayout = new VerticalLayout();
        footerLayout.setHeight("200px");




//        if ( repository != null ) {
//            repository.save(new Customer("email@cvut.cz", "lovesi1", "Honza", "mylogin4", "+420776000000"));
//            repository.save(new Customer("email@cvut.cz", "lovesi1", "Honza", "mylogin4", "+420776000000"));
//            repository.save(new Customer("email@cvut.cz", "lovesi3", "Honza", "mylogin", "+420776000000"));
//            repository.save(new Customer("email@cvut.cz", "lovesi5", "Honza", "mylogin", "+420776000000"));
//            repository.save(new Customer("email@cvut.cz", "lovesi6", "Honza", "mylogin", "+420776000000"));
//        } else {
//            log.info("Repo error");
//        }

//        for (Customer cust : repository.findByEmail("email@cvut.cz")) {
//            log.info(cust.toString());
//        }

        emailField.setPlaceholder("enter e-mail");
        emailField.setRequiredIndicatorVisible(true);
        emailField.setWidth(widthPercentage);

        passwordField.setPlaceholder("enter password");
        passwordField.setRequired(true);
        passwordField.setWidth(widthPercentage);

        userTypeSelect.setItems(userTypes);
        userTypeSelect.setLabel("I am here as:");
        userTypeSelect.setRequired(true);
        userTypeSelect.setValue(userTypes[0]);

        loginButton.setWidth(widthPercentage);
        loginButton.setEnabled(false);
        loginButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        registerButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

        add(footerLayout, emailField, passwordField, userTypeSelect, loginButton, registerButton);
    }

    @Override
    public void setupListeners() {
        userTypeSelect.addValueChangeListener(e -> {
            if (  emailIsValid(emailField.getValue()) && passwordIsValid(passwordField.getValue())) {
                loginButton.setEnabled(true);
            } else {
                loginButton.setEnabled(false);
            }
        });

        emailField.addValueChangeListener(e -> {
            if ( emailIsValid(emailField.getValue()) ) {
                emailField.setInvalid(false);
                if ( passwordIsValid(passwordField.getValue()) ) {
                    loginButton.setEnabled(true);
                } else {
                    loginButton.setEnabled(false);
                }
            } else {
                emailField.setInvalid(true);
                loginButton.setEnabled(false);
            }

        });

        passwordField.addValueChangeListener(e -> {
           if ( !passwordIsValid(passwordField.getValue()) ) {
                passwordField.setInvalid(false);
                if ( emailIsValid(emailField.getValue()) ) {
                    loginButton.setEnabled(true);
                } else {
                    loginButton.setEnabled(false);
                }
           } else {
               passwordField.setInvalid(true);
               loginButton.setEnabled(false);
           }
        });

        loginButton.addClickListener(e -> {

        });

        registerButton.addClickListener(e -> {
            registerButton.getUI().ifPresent(ui -> ui.navigate("register"));
        });

    }
}
