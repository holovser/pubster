package com.fit.pubster.config;

import com.fit.pubster.PubsterApplication;
import com.fit.pubster.repository.CustomerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories(basePackageClasses = {PubsterApplication.class})
public class PubsterConfig {

//    @Bean
//    public DataSource getDataSource() {
//        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
////        dataSourceBuilder.driverClassName("org.h2.Driver");
//        dataSourceBuilder.url("jdbc:postgresql://localhost:5432/pubster");
//        dataSourceBuilder.username("pubster");
//        dataSourceBuilder.password("PUBster");
//
//
//
//        return dataSourceBuilder.build();
//    }

}

